Git Workflow
============


Overview
--------

The workflow described in this document is heavily inspired by the workflow
proposed by Vincent Driessen in ["A successful Git branching model"][1] (aka
the "gitflow workflow").

This workflow (hopefully) has the following desirable properties:
- it provides an efficient collaboration framework
- it facilitates code reviews
- it is CI/CD friendly

This workflow is built around a central repository used as a communication and
collaboration hub.
In essence, it is only a refinement over a more commonly used "feature branch"
workflow (for more information and an in-depth description of some commonly
used git workflows, see ["Comparing Workflows"][2]). It differs by defining a
strict branching model where each branch has a precise and well defined purpose
and by enforcing how (and when) they interact with each other.


Branches
--------

The different branches will be described in details in the following sections.

### Development branch
This branch represents the current development state of the project and serves
as an integration branch for new features.
The `master` branch will be used for this purpose. We'll use "_master_ branch" and
"_development_ branch" interchangeably throughout this document.

### Production branch
The production branch stores the project's release history and contains only
tagged commits.

### Feature branches
A feature branch must be used to keep track of the development of a particular
feature. These branches can be pushed to the central repository for backup and
collaboration.
Feature branches must be branched off of the *master* branch. When a feature is
considered complete (and fulfills the project's definition of done), a merge
request to the `master` branch is made. Feature branches must never interact
with another branch.
The goal is to **maintain the *master* branch as stable and consistent** as possible
by atomically integrating features that meet the project's quality requirements.

### Staging branches
Once development branch has accumulated enough features for a release, a staging
branch must be created by branching off development branch. No new features can
be added in these branches: only bug fixes, documentation, and other simple
release-oriented updates.
When the staging branch is finally ready for the release, a merge request to the
production branch is made. Once the request has been reviewed and approved, the
staging branch gets merged and the production branch is tagged with a new
version number.
Additionally, the staging branch must also be merged back into the development
branch, which may have been updated since the staging branch was created.
Using a dedicated branch to prepare releases makes it easy to continue working
on the development branch while polishing the next release.

### Maintenance branches
Maintenance branches are created from a specific tag in the production branch.
They must only contain urgent fixes that can't wait the next release.
Once fixed, the maintenance branch gets merged (again, a merge request must be
made beforehand) into production branch and tagged with a new version number.
In addition, it must also be merged into the development and (eventually) the
current staging branches so that the fixes are also included in the codebase for
future releases.

### Support branches
Those are special branches, semantically similar to the production branch but that
allow to further publish fixes to former versions of the product.
As the production branch, they hold the commits contained in a specific version of
the product.  
_Usage : They are particularly useful for libraries, where we tend to change version
each time a breaking change is introduced. From then on, without support branches, any
bugfix publication implies for the users to integrate and adapt to all those changes
in the same time._


Interaction and merge rules
---------------------------

It must not be possible to directly push commits to development and production /
support branches. These branches must only accept commits via merge requests.
Merge requests must be reviewed and processed by a different developer than the
developer having made the request (i.e. it must not be possible for a single
person to push commits without a peer review into development and production
branches).


Naming conventions
------------------

Note : *version* is represented as *major.minor.patch*.
- Staging branches: `staging-*version*`
- Maintenance branches: `hotfix-*version*`
- Feature branches: `*JIRA-KEY*-*description*` _(The description must be as synthetic
  as possible, yet precise enough)_
- Support branches: `support-*version*`
- Tags: `*version*`


Additional requirements
-----------------------

- Merge requests, when accepted, must result in a merge commit (i.e. merge is
  preferred over rebase, for a more in-depth discussion on this particular topic,
  see ["Merging vs. Rebasing"][3]).
- Merged branches must be deleted on a regular basis.
- Branches interaction and merge rules as described previously must be enforced
  in gitlab.
- All commits must be signed.
- Two factor authentication must be enabled on gitlab for repository
  administration tasks.



 [1]: http://nvie.com/posts/a-successful-git-branching-model
 [2]: https://www.atlassian.com/git/tutorials/comparing-workflows
 [3]: https://www.atlassian.com/git/tutorials/merging-vs-rebasing
