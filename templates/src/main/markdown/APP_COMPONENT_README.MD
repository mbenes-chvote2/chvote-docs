# Component name

Component short description, including role in the application and technologies and framework used

# Modules
Sub-modules list, and for each, a short description of its purpose

# Development guidelines
Reference to applicable development directives from chvote-docs

# Building
- pre-requisites, if any
- build commands
- where to find tests reports
- artifacts produced by the build

# Running
Describe how to run locally
- explain any useful profiles / env var
- pre-requisites (ie database or RabbitMQ dependency)
- commands
- url and port of application
- if authentication is required, list of user/password available

# Tips
Any useful tips